<div align="right">Engineering Logbook</div>

<div align="right">Department of Aerospace Engineering </div>
<div align="right">Faculty of Engineering</div>
<div align="right">Universiti Putra Malaysia</div>

**Name:** Ummi Zuhairah Binti Zaimi
###
**Matric No.:** 196659
###
**Team:** Three

**Subsystem:** Simulation and CFD
***
### Table of Contents
[01](#logbook-01)

[02](#logbook-02)

[03](#logbook-03)

[04](#logbook-04)

[05](#logbook-05)

[06](#logbook-06)

[07](#logbook-07)

[08](#logbook-08)

[09](#logbook-09)

[10](#logbook-10)
***
## Logbook 01

> ### **Agenda**
- Plan and create a *[Gantt Chart](https://docs.google.com/spreadsheets/d/1j9F4gbNZMCxQDzfhFpA2i7lVFxfhL9kVc-zxi1asfZs/edit#gid=1115838130)* (by Week 8)  which can be accessed by everyone. 
- Divide the tasks that need to be completed by Week 8
###
> ### **Goals** 
- To make sure the tasks can be completed within the given period 
- To ensure everyone is alert with the time and manage the task efficiently
- Everyone have a specific task to work and focus on
###
> ### **Decisions**
 After the briefing from the supervisor, we decided to plan on the flow of the project. We have identify three main focus for the subsystem (Simulation) which are CATIA Design, Performance Calculation and CFD. Therefore, as discussed, we break ourselves into three smaller groups.
 ###
> ### **Method and Justifications**
- Considering the complexity of the task, a suitable period have been discussed and decided in the *[Gantt Chart](https://docs.google.com/spreadsheets/d/1j9F4gbNZMCxQDzfhFpA2i7lVFxfhL9kVc-zxi1asfZs/edit#gid=1115838130)* to ensure everyone is alert with the time and able to manage the task efficiently.
- The specific tasks were assigned to the group members based on their skills and passion in order to make sure the tasks can be completed at the right time.

| Task | Member |
| ------ | ------ |
| CATIA Design |  Haziq, Aeinuddin & Yousef |
| Performance Calculation | Viknes, Kee Soon Win & Adrian |
| CFD | Liyana, Najwa & Ummi |
###
> ### **Impact** 
- Able to plan the flow of the project and prepare earlier with what need to be done next.
- Get to apply engineering knowledge gained before such as Engineering Drawing, Fixed Wing Design and Computational Fluid Dynamics in solving the tasks.
###
>### **Next step**
- Discuss on when and who is going to the lab for the measurement of the airship.
- Proceed with the CATIA design

[Table of Contents](#table-of-contents)
***
## Logbook 02


> ### **Agenda** 
>> **Team**
- Appoint a Checker, Process Monitor, Coordinator and Recorder among the team member.
- Help each other to work on the engineering logbook 
>> **Subsystem**
-  Discuss on the progress of Solid Work drawing of the airship. 
###
> ### **Goals**
>> **Team**
- To make sure the meeting can be proceeded smoothly with the assistant of the management roles.
- To ensure that all team member have completed their engineering logbook and submitted properly.
>> **Subsystem**
- To inform the member of the progress of the drawing.
###
> ### **Decisions**
 >> **Team**
- Haikal was chosen as the Coordinator which will guide the team throughout the process. As roles for the Checker, Process Monitor and Recorder, the rest of the team member will take turn for each week based on the list.
- Problems related to the content and how to submit the engineering logbook were discussed among the member.
>> **Subsystem**
- The smaller team which has been assigned for the Drawing explain their progress to the other.
 ###
> ### **Method and Justifications**
>> **Team**
- Members will take turn for the management roles so that everyone can experience on handling the role.
- The discussion was done to ensure that everyone have completed the logbook properly.
>> **Subsystem**
- The brief meeting was done so that everyone are on track with the progress of the project.
###
> ### **Impact**
>>**Team**
- Responsible towards the task given and also towards the team member, so that no one were left behind.
>> **Subsystem**
- No procrastination during completing the task assigned.
>### **Next step**
>>**Team**
- Team members will be given a new list of management role.
- Correction for the engineering logbook need to be done for those who did not complete it properly.
>> **Subsystem**
- The Drawing team will proceed on the airship drawing, the best design will be chosen for the analysis.

[Table of Contents](#table-of-contents)
***
## Logbook 03

###
> ### **Agenda** 
>> **Team**
- Appoint a Checker, Process Monitor, Coordinator and Recorder among the team member.
>> **Subsystem**
- Presentation of the chosen drawing of the airship.
-  Review the Chapter 2 (Aerostatics) and 3 (Aerodynamics) in the Fundamentals of Aircraft and Airship Design. 

###
> ### Goals 
>> **Team**
- To make sure the meeting can be proceeded smoothly with the assistant of the management roles.
>> **Subsystem**
- To inform the member of the progress of the drawing and the best drawing out of three.
###
> ### Decisions
 >> **Team**
- Haikal was chosen as the Coordinator which will guide the team throughout the process. As roles for the Checker, Process Monitor and Recorder, the rest of the team member will take turn for each week based on the list.
- A schedule in Excel was done so that it will be more manageable and everyone will be able to take note on their responsibilities.
- Kisshore was unable to join the meeting and his role was replaced by the other member.
>> **Subsystem**
- The smaller team which has been assigned for the Drawing explain their progress to the other.
 ###
> ### Method and Justifications
>> **Team**
- Members will take turn for the management roles so that everyone can experience on handling the role.
- The discussion was done to ensure that everyone have completed the logbook properly.
- If any of the member was unable to join the meeting, the role will be passed to someone else and the list will be forwarded for next week.
>> **Subsystem**
- The brief meeting was done so that everyone are on track with the progress of the project.
###
> ### Impact 
>>**Team**
- Responsible towards the task given and also towards the team member, so that no one were left behind.
>> **Subsystem**
- The task can be completed within the right period of time.
>### Next step
>>**Team**
- Team members will be given a new list of management role.
>> **Subsystem**
- The CFD Simulation team will need to proceed on the simulation of the airship.

[Table of Contents](#table-of-contents)
***
## Logbook 04

> ### **Agenda**
>> **Team**
- Appoint a Checker, Process Monitor, Coordinator and Recorder among the team member.
>> **Subsystem**
- the Simulation team starts to familiarize themselves with Ansys simulation software.
- the Performance Calculation team discuss on the parameter needed to calculate the performance of the airship.

###
> ### **Goals**
>> **Team**
- To make sure the meeting can be proceeded smoothly with the assistant of the management roles.
>> **Subsystem**
- To understand how the software works and the output provided by the software.
###
> ### **Decisions**
 >> **Team**
- Roles for the Checker, Process Monitor and Recorder, the rest of the team member will take turn for each week based on the list.
- A schedule in Excel was done so that it will be more manageable and everyone will be able to take note on their responsibilities.
>> **Subsystem**
- Simulation team joined a crash course for Ansys guided by Mr. Hidayat Maddali.
- Managed a list of parameters needed from other subsystems.
 ###
> ### **Method and Justifications**
>> **Team**
- Members will take turn for the management roles so that everyone can experience on handling the role.
>> **Subsystem**
- Besides joining the crash course, the cfd simulation were done based on tutorials and guide from the Youtube and helps from the classmates.
- In order to make sure that the simulation are valid, research are done by referring some books and videos from the Internet. We also refer to someone who are well-versed with the CFD Simulation.
###
> ### **Impact**
>>**Team**
- Responsible towards the task given and also towards the team member, so that no one were left behind.
>> **Subsystem**
- Within less than a week, the Simulation team are able to mesh and provide a static simualtion of the airship. 
>### **Next step**
>>**Team**
- Team members will be given a new list of management role for next meeting.
>> **Subsystem**
- The CFD Simulation team will need to proceed on the complete simulation of the airship.
- Performance Calculation team will start to calculate the performance of the airship using the result provided by the Simulation team.

[Table of Contents](#table-of-contents)

## Logbook 05

> ### **Agenda**
>> **Subsystem**
- the Simulation team starts with simulating the airship at different inlet velocity to find the aerodynamics coefficient such as Lift coefficient and Drag coefficient.
- the Performance Calculation team works on the plot of Volume vs Weight graph.

###
> ### **Goals**
>> **Subsystem**
- To provide the data needed by the Propulsion Group .
- To estimate the volume of the airship.
###
> ### **Decisions**
>> **Subsystem**
- Simulation team gathered at the lab to complete the simulation together as some of us having problems to run the Ansys software due to hardware capability.
- Asked guidance from Mr. Fikri on the simulation of the airship.
- Calculation team tried to plot the graph based on the weight from each respective subsystem and the proper formula.

 ###
> ### **Method and Justifications**
>> **Subsystem**
- As the computation takes a lot of time, we take turn to handle the meshing, setup and data management.
- The results were carefully saved in a document for future reference and further use.
- The results were then justified with Dr. Salah, however the setup of the simulation need to be validate to ensure the results are valid. Therefore, another simulation of 2d airship need to be done and compare  the trend with the one in the book.
###
> ### **Impact**
>> **Subsystem**
- The simulation was successfully completed with the cooperation of the team and high performance laptop.
- Personally, I was unable to complete the whole process of CFD due to the problem of my laptop. However, it is good to be able to learn and complete the whole process of CFD at the lab with the guide from the team members, thanks to Haziq's powerful laptop and Simulation team.

>### **Next step**
>> **Subsystem**
- The CFD Simulation team will need to proceed on the 2d simulation of the airship.
- Performance Calculation team will start to calculate the size of the airship.

[Table of Contents](#table-of-contents)

## Logbook 06

> ### **Agenda**
>> **Subsystem**
- the Simulation team starts with simulating the 2d airship at different angle of attack starting from -5deg to 20deg to find the aerodynamics coefficient such as Lift coefficient and Drag coefficient.
- Compare the trend of the plot with the one in the book.
- the Performance Calculation team works on the size of the airship, specifically length of the airship .

###
> ### **Goals**
>> **Subsystem**
- To validate the setup of the simulation .
- To estimate the volume of the airship.
###
> ### **Decisions**
>> **Subsystem**
- The task were divided for 5 people due to the long time taken of computation. However, some of us still tried to do all the simulations of different angle of attack.
- Calculation team tried to find the applicable formula to find the size of the airship.
 ###
> ### **Method and Justifications**
>> **Subsystem**
- As the computation takes a lot of time, the points were divided among the members.
- The results gained from the simulation are gathered in an Excel spreadsheet to be analysed and to compare which result is better.
- During the progress presentation, we just realized that there was a misunderstanding which is initially we heard that the trend of cl versus aoa graph of the 2d HAU airship need to be compared with the cl vs aoa graph in the book. However, actually we need to simulate the airship in the book and compare if the results gained will be the same. 
###
> ### **Impact**
>> **Subsystem**
- Team members are able to try the simulation of 2d airship on their own as the meshing is not as complex as the 3d model.
- Lessons learnt are to listen carefully and focus on the aim of the task.

>### **Next step**
>> **Subsystem**
- The CFD Simulation team will need to proceed on the 2d simulation of the airship based on the model in the book.
- CATIA team will draw the 2d airship following the parameter provided as in the book.
- Performance Calculation team need to weigh the airship skin.

[Table of Contents](#table-of-contents)
***
## Logbook 07

> ### **Agenda**
>> **Subsystem**
- the Simulation team need to simulate the 2d airship at different angle of attack to find the aerodynamic coefficients such as Lift coefficient and Drag coefficient based on previous study.
- Validate the simulation results.
- the Performance Calculation team need to record the weight of the airship skin at the lab.

###
> ### **Goals**
>> **Subsystem**
- To validate the setup of the simulation .
- To estimate the volume of the airship.

###
> ### **Decisions**
>> **Subsystem**
- NACA0012 was chosen and the task were divided for 5 people due to the long time taken of computation. However, some of the team members still tried to do all the simulations of different angle of attack as apart of the members faced some difficulty on completing the mesh part.

 ###
> ### **Method and Justifications**
>> **Subsystem**
- As the computation takes a lot of time, the points were divided among the members.
- As the result gained from the simulation of NACA0012 are acceptable, the simulation for the airship is then can be proceed using the same setup.

###
> ### **Impact**
>> **Subsystem**
- Team members are able to try the simulation of 2d airship on their own however there are some of the members were not able to complete the simulation due to the different techniques/setup.
- Lessons learnt is to discuss clearly with everyone on how to complete a specific task and work as a team.

>### **Next step**
>> **Subsystem**
- The CFD Simulation team will need to proceed on the 2d simulation of the airship using the valid setup.



[Table of Contents](#table-of-contents)

## Logbook 08

> ### **Agenda**
- Prepare an individual resume.

###
> ### **Goals**
- To complete an individual resume.

###
> ### **Decisions**
- a simple and professional resume template is use.

 ###
> ### **Method and Justifications**
- Create a resume based on a template in Canva
- Using a chronological resume format for presenting the order of activities or experiences.

###
> ### **Impact**
- Able to present skills and experiences gained in a piece of short yet simple resume. 

>### **Next step**
- Prepare for subsystem report.



[Table of Contents](#table-of-contents)

## Logbook 09

> ### **Agenda**
>> **Subsystem**
- Complete the subsystem report as briefed by the FSI subsystem.

###
> ### **Goals**
>> **Subsystem**
- To complete the subsystem report following the format given.

###
> ### **Decisions**
>> **Subsystem**
- Each member has been assigned into smaller topic such as Introduction, Methodology, Results and Discussion and conclucion.

 ###
> ### **Method and Justifications**
>> **Subsystem**
- Based on the divided small team, every member need to complete their part in the report.
- All the result and data need to be simplified and presented in the report.

###
> ### **Impact**
>> **Subsystem**
- The simulation process from start till the end are discussed and summarized in the report.
- Every member need to work together and keep the report as neat as possible.
>### **Next step**
>> **Subsystem**
- Submit to the FSI subsystem for compilation of the whole report including all subsystems.



[Table of Contents](#table-of-contents)

## Logbook 10 

> ### **Agenda**
- Measure the weight of the airship.
- Help other subsystems in completing the installation of the propulsion system, payload, thruster arm and etc.

###
> ### **Goals**
- To estimate the Helium needed to fill in the airship.
- To complete the installation on the airship.

###
> ### **Decisions**
- Everyone works together in order to make the process of weighing goes smoothly.
- The other subsystem (e.g; Design subsystem) guide the others to assist with the incompleted tasks.

 ###
> ### **Method and Justifications**
- The airship was hang to the weighing scale in order to measure the weight. Next, the same method was used to measure the weight of the overall airship (including payload). 
- All the data need is recorded.

###
> ### **Impact**
- As there were too many students in the lab, and also too many students handling a task, there were some misunderstanding and confusion happened. However, double check and discussions are done to solve the problem.

[Table of Contents](#table-of-contents)
